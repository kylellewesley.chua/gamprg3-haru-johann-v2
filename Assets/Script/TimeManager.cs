﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class TimeManager : MonoBehaviour
{

    public enum TimeState
    {
        Day,
        Night
    }

    public TimeState tState;

    float currentTime;

    public float timeCoeff;
    public int seconds;
    public int minutes;


    int currentMinute;


    public int fiveMinIncrement;
    Light directionalLight;
    public GameObject dirLightGameObject;

    private void Awake()
    {
        SingletonManager.Register<TimeManager>(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        currentTime = 0;
        currentMinute = 0;
        timeCoeff = 1;
        tState = TimeState.Day;
        fiveMinIncrement = 0;
        directionalLight = dirLightGameObject.GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += (Time.deltaTime * timeCoeff);
        UpdateTime();
    }

    void UpdateTime()
    {
        minutes = Mathf.FloorToInt(currentTime / 60);
        seconds = Mathf.FloorToInt(currentTime % 60);

        CheckTime();
    }

    void CheckTime()
    {
        
        if (currentMinute < minutes)
        {
            currentMinute = minutes;
            int fiveIncrementHolder = (currentMinute / 5);

            if(fiveMinIncrement < fiveIncrementHolder)
            {
                
                fiveMinIncrement = fiveIncrementHolder;
                ChangeLight();
            }
        }

    }

    void ChangeLight()
    {
        if((fiveMinIncrement % 2) == 0)
        {
            tState = TimeState.Day;
            directionalLight.color = new Color((240.0f / 255.0f), (255.0f / 255.0f), (155.0f / 255.0f), 1.0f);
        }

        else if ((fiveMinIncrement % 2) == 1)
        {
            tState = TimeState.Night;
            directionalLight.color = new Color((60.0f / 255.0f), (95.0f / 255.0f), (155.0f / 255.0f), 1.0f);
        }


    }

}
