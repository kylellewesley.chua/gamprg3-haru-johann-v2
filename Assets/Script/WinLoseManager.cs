﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinLoseManager : MonoBehaviour
{
    public void PauseTimer()
    {
        Time.timeScale = 0;
    }

    public void LoadScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
