﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Mana : MonoBehaviour
{
    [SerializeField] private float manaCurrent;
    [SerializeField] private float manaMax;
    [SerializeField] private float mpPercentage;
    [SerializeField] private RectTransform manaUI;
    [SerializeField] TextMeshProUGUI textManaCurrent;
    [SerializeField] TextMeshProUGUI textManaMax;

    private Canvas canvasUI;

    public event Action OnSelected;

    private void Awake()
    {
        UIReferenceSetup();

        if (this.GetComponent<Creep>())
        {
            manaMax = GetComponent<Creep>().GetMana();
            manaCurrent = manaMax;
            mpPercentage = manaCurrent / manaMax;
        }

        if (this.GetComponent<PlayerController>())
        {
            manaCurrent = manaMax;
            mpPercentage = manaCurrent / manaMax;
            manaUI.localScale = new Vector3(mpPercentage, 1, 1);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateMana()
    {
        if (this.GetComponent<Health>().IsSelected)
        {
            if (manaCurrent <= 0)
                manaCurrent = 0;
            mpPercentage = manaCurrent / manaMax;
            int current = (int)manaCurrent;
            int max = (int)manaMax;
            textManaCurrent.text = current.ToString();
            textManaMax.text = max.ToString();
            if (manaMax != 0)
                manaUI.localScale = new Vector3(mpPercentage, 1, 1);
            else
                manaUI.localScale = new Vector3(0, 1, 1);
            this.OnSelected?.Invoke();
        }
    }

    private IEnumerator ConstantManaRegen()
    {
        while (manaCurrent > 0)
        {
            ManaRegen();
            yield return new WaitForSeconds(1f);
        }

    }

    private void UIReferenceSetup()
    {
        canvasUI = FindObjectOfType<Canvas>();
        manaUI = canvasUI.transform.Find("ManaBar").GetComponent<RectTransform>();
        textManaCurrent = canvasUI.transform.Find("manaText")
            .transform.Find("manaTemplate")
            .transform.Find("manaCurrent").GetComponent<TextMeshProUGUI>();
        textManaMax = canvasUI.transform.Find("manaText")
            .transform.Find("manaTemplate")
            .transform.Find("manaMax").GetComponent<TextMeshProUGUI>();
    }


    private void ManaRegen()
    {
        if (manaCurrent < manaMax)
        {
            
            mpPercentage = manaCurrent / manaMax;
        }
        else
        {
            manaCurrent = manaMax;
        }
    }

    public void ManaUsage(float manaCost)
    {
        manaCurrent -= manaCost;
        mpPercentage = manaCurrent / manaMax;
    }

    public float GetCurrentMana()
    {
        return manaCurrent;
    }

}
