﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
using System;

public class AIState
{
    public enum State
    {
        Idle,
        Attack,
        Running
    }
}


public class AIController : MonoBehaviour
{
    [SerializeField] AIState.State state;
    [SerializeField] Transform targetTransform;
    [SerializeField] List<Waypoints> waypoints = new List<Waypoints>();
    [SerializeField] float targetMaxRadius = 20f;
    [SerializeField] float turnRate = 0.1f;
    [SerializeField] float attackRange = 1.5f;
    [SerializeField] float attackRate = 2f;
    [SerializeField] TextMeshProUGUI damageText;
    [SerializeField] TextMeshProUGUI armorText;

    private Canvas canvasUI;

    private bool canAttack = true;
    private int currentIndex = 0;
    private float tempDistance;
    private bool hasWaypoint = false;
    private Waypoints currentWaypoint;

    public AIState.State State { get { return state; } set { state = value; } }
    public Transform TargetTransform { get { return targetTransform;  } set { targetTransform = value; } }
    public bool hasTarget;

    [SerializeField] NavMeshAgent agent;
    [SerializeField] Animator anim;

    private Health health;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        health = GetComponent<Health>();
        health.OnSelected += Health_OnSelected;

    }

    private void OnDestroy()
    {
        health.OnSelected -= Health_OnSelected;
    }

    private void Health_OnSelected()
    {
        UpdateDamageUI();
        UpdateArmorUI();
    }

    private void Start()
    {
        WaypointManager waypointManager = SingletonManager.Get<WaypointManager>();

        switch (GetComponent<Creep>().GetFaction() == FactionType.FactionTypes.Radiant)
        {
            case true:
                switch (GetComponent<Creep>().GetLane())
                {

                    case SpawnerLane.SpawnLane.MID:
                        waypoints = waypointManager.GetRadiantMidWaypoints();
                        break;
                    case SpawnerLane.SpawnLane.TOP:
                        waypoints = waypointManager.GetRadiantTopWaypoints();
                        break;
                    case SpawnerLane.SpawnLane.BOT:
                        waypoints = waypointManager.GetRadiantBotWaypoints();
                        break;
                }
                break;

            case false:
                switch (GetComponent<Creep>().GetLane())
                {

                    case SpawnerLane.SpawnLane.MID:
                        waypoints = waypointManager.GetDireMidWaypoints();
                        break;
                    case SpawnerLane.SpawnLane.TOP:
                        waypoints = waypointManager.GetDireTopWaypoints();
                        break;
                    case SpawnerLane.SpawnLane.BOT:
                        waypoints = waypointManager.GetDireBotWaypoints();
                        break;
                }
                break;

        }

        if (currentWaypoint == null)
        {
            UpdateWaypointDestination(currentIndex);
        }

        UIReferenceSetup();
    }

    private void Update()
    {
        

        if (targetTransform == null)
        {
            hasTarget = false;
            state = AIState.State.Idle;
            UpdateWaypointDestination(currentIndex);
        }
            

        if (state == AIState.State.Idle)
        {
            CheckForTarget(targetTransform);
            if (targetTransform == null)
                LookForTarget();
        }

        if (hasTarget)
        {
            
            ChaseAndAttack();
        }
        else
        {
            FollowWaypoint();
        }
        
      
    }

    public void UpdateDamageUI()
    {
        int damage = (int)GetComponent<Creep>().GetDamage();
        damageText.text = damage.ToString();
    }

    public void UpdateArmorUI()
    {
        int armor = (int)GetComponent<Health>().Armor;
        armorText.text = armor.ToString();
    }

    private void UIReferenceSetup()
    {
        canvasUI = FindObjectOfType<Canvas>();
        damageText = canvasUI.transform.Find("damageText")
            .transform.Find("damageTemplate")
            .transform.Find("damageAmount").GetComponent<TextMeshProUGUI>();
        armorText = canvasUI.transform.Find("armorText")
            .transform.Find("armorTemplate")
            .transform.Find("armorAmount").GetComponent<TextMeshProUGUI>();
    }

    private void FollowWaypoint()
    {
        var distance = Vector3.Distance(currentWaypoint.transform.position, transform.position);
        if (distance < 2f)
        {
            if(currentIndex < waypoints.Count - 1)            
                currentIndex++;

            UpdateWaypointDestination(currentIndex);

        }
    }

    private void UpdateWaypointDestination(int index)
    {
        currentWaypoint = waypoints[index];
        agent.SetDestination(currentWaypoint.transform.position);
    }

    private void ChaseAndAttack()
    {
        agent.SetDestination(targetTransform.position);
        var distance = Vector3.Distance(targetTransform.position, this.transform.position);
        if (distance <= attackRange)
        {
            transform.LookAt(targetTransform);
            state = AIState.State.Attack;

            if (canAttack)
                StartCoroutine(Attack());

            agent.isStopped = true;
            agent.ResetPath();
        }
        else if(distance > targetMaxRadius)
        {
            targetTransform = null;
        }
        else
        {
            
            anim.SetTrigger("Running");
            agent.isStopped = false;
        }
    }

    private IEnumerator Attack()
    {
        anim.SetTrigger("Running");
        canAttack = false;
        anim.SetTrigger("Attack");
        yield return new WaitForSeconds(attackRate);
        canAttack = true;
    }

    public void CanDamage()
    {
        if(targetTransform != null)
        {
            var myTarget = targetTransform;
            var distance = Vector3.Distance(myTarget.position, transform.position);
            if (distance <= attackRange)
            {
                DealDamage(myTarget);
            }
        }
      
    }

    public void DealDamage(Transform target)
    {
        var enemyHealth = target.GetComponent<Health>();
        if (enemyHealth)
        {
            var myDamage = GetComponent<Creep>().GetDamage();
            enemyHealth.OnDamage(myDamage, this.gameObject);
            
            
        }
        
    }

 
    public void CheckForTarget(Transform target)
    {
        if (target != null)
        {
            hasTarget = true;
            Vector3 direction = target.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, turnRate * Time.deltaTime);
        }
        else
        {
            hasTarget = false;
            LookForTarget();
        }
    }


    public void LookForTarget()
    {

        Collider[] colliderArray = Physics.OverlapSphere(transform.position, targetMaxRadius);


        foreach (Collider collider in colliderArray)
        {
            PlayerController enemyPlayer = collider.GetComponent<PlayerController>();
            Creep creep = collider.GetComponent<Creep>();
            Buildings building = collider.GetComponent<Buildings>();

            CheckHeroesNearby(enemyPlayer);
            CheckBuildingsNearby(building);
            CheckCreepsNearby(creep);


        }
    }

    private void CheckBuildingsNearby(Buildings building)
    {
        if (building != null)
        {
            if (building.GetFactionType() != this.gameObject.GetComponent<Creep>().GetFaction())
            {
                if (targetTransform == null)
                {
                    if (building.GetIsVunerable())
                    {
                        targetTransform = building.transform;
                    }

                }
                else
                {
                    //change to the closest target
                    if (Vector3.Distance(transform.position, building.transform.position) <
                    Vector3.Distance(transform.position, targetTransform.position))
                    {
                        targetTransform = building.transform;
                        Debug.Log("Building located: " + targetTransform);
                    }
                }
            }
        }
    }


    private void CheckHeroesNearby(PlayerController enemyPlayer)
    {
        if (enemyPlayer != null)
        {
            //Check if they are not on the same faction
            if (enemyPlayer.Faction != this.gameObject.GetComponent<Creep>().GetFaction())
            {
                if (targetTransform == null)
                {
                    targetTransform = enemyPlayer.transform;
                }
                else
                {
                    //change to the closest target
                    if (Vector3.Distance(transform.position, enemyPlayer.transform.position) <
                        Vector3.Distance(transform.position, targetTransform.position))
                    {
                        targetTransform = enemyPlayer.transform;
                    }
                }
            }
        }
    }


    private void CheckCreepsNearby(Creep creep)
    {
        if (creep != null)
        {
            //Check if they are not on the same faction
            if (creep.GetFaction() != this.gameObject.GetComponent<Creep>().GetFaction())
            {
                if (targetTransform == null)
                {
                    targetTransform = creep.transform;
                }
                else
                {
                    //change to the closest target
                    if (Vector3.Distance(transform.position, creep.transform.position) <
                        Vector3.Distance(transform.position, targetTransform.position))
                    {
                        targetTransform = creep.transform;
                    }
                }
            }
        }
    }

}
