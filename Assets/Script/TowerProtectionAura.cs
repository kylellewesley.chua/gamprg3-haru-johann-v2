﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TowerProtectionAura : MonoBehaviour
{
    [SerializeField] float bonusArmor = 3;
    [SerializeField] FactionType.FactionTypes faction;
    [SerializeField] TextMeshProUGUI bonusArmorText;


    private void OnTriggerStay(Collider other)
    {
        var otherArmor = other.GetComponent<Health>();
        if (otherArmor)
        {
            if (otherArmor.Faction == faction && !otherArmor.HasProtectionBuff)
            {
                otherArmor.IncreaseArmor(bonusArmor);
                otherArmor.HasProtectionBuff = true;
            }
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var otherArmor = other.GetComponent<Health>();
        if (otherArmor)
        {
            if (otherArmor.Faction == faction && otherArmor.HasProtectionBuff)
            {
                otherArmor.DecreaseArmor(bonusArmor);
                otherArmor.HasProtectionBuff = false;
            }
            
        }
    }
}
