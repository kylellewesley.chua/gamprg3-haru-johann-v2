﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class CreepType
{
    public enum CreepTypes
    {
        Melee,
        Range,
        Siege
    };
}

[SerializeField]
public class CreepPower
{
    public enum CreepPowers
    {
        Normal,
        Super
    };
}

[SerializeField]
public class FactionType
{
    public enum FactionTypes
    {
        Radiant,
        Dire
    };
}

[SerializeField]
public class AttackType
{
    public enum Types
    {
        Basic,
        Pierce,
        Siege,
        Hero
    };
}

[SerializeField]
public class ArmorType
{
    public enum Types
    {
        Basic,
        Fortified,
        Hero
    };
}



public class Creep : MonoBehaviour
{
    [Header("Creeps Base Stats")]
    [SerializeField] private SpawnerLane.SpawnLane lane;
    [SerializeField] private FactionType.FactionTypes faction;
    [SerializeField] private AttackType.Types attackType;
    [SerializeField] private ArmorType.Types armorType;
    [SerializeField] private CreepPower.CreepPowers powerType;
    [SerializeField] private CreepType.CreepTypes types;
    [SerializeField] private float health = 550f;
    [SerializeField] private float damage = 40f;
    [SerializeField] private float mana = 0;
    [SerializeField] private float armor = 3f;
    [SerializeField] private float statGain = 0.05f;
    [SerializeField] private int upgradeCount = 0;
    [SerializeField] private float bountyReward = 40f;
    [SerializeField] private float expReward = 25f;


    protected virtual void Attack()
    {
        Debug.Log("Attack");
    }

    protected virtual void Idle()
    {
        Debug.Log("Idle");
    }

    protected virtual void Running()
    {
        Debug.Log("Running");
    }






    #region Properties
    public CreepType.CreepTypes GetCreepType()
    {
        return types;
    }

    public FactionType.FactionTypes GetFaction()
    {
        return faction;
    }

    public AttackType.Types GetAttackType()
    {
        return attackType;
    }

    public ArmorType.Types GetArmorType()
    {
        return armorType;
    }

    public float GetHealth()
    {
        if (upgradeCount <= 0)
        {
            return health;
        }
        else
        {
            return (health + (health * (GetStatGain())));
        }

    }

    public float GetDamage()
    {
        if (upgradeCount <= 0)
        {
            return damage;
                //Random.Range(damage - 4, damage);
        }
        else
        {
            return damage + (damage * (GetStatGain()));
                //Random.Range(damage + (damage * (GetStatGain())) - 4, damage + (damage * (GetStatGain())));
        }

    }

    public float GetMana()
    {
        if (upgradeCount <= 0)
        {
            return mana;
        }
        else
        {
            return mana + (mana * (GetStatGain()));
        }

    }

    public float GetArmor()
    {
        if (upgradeCount <= 0)
        {
            return armor;
        }
        else
        {
            return armor;
        }

    }

    public float GetBountyReward()
    {
        if (upgradeCount <= 0)
        {
            return Random.Range(bountyReward - 10, bountyReward);
        }
        else
        {
            return Random.Range(bountyReward + (bountyReward * (GetStatGain())) - 10, bountyReward + (bountyReward * (GetStatGain())));
        }

    }

    public float GetExpReward()
    {
        if (upgradeCount <= 0)
        {
            return expReward;
        }
        else
        {
            return expReward + (expReward * (GetStatGain()));
        }

    }

    public void SetUpgradeCount(int value)
    {
        upgradeCount = value;
    }

    public int GetUpgradeCount()
    {
        return upgradeCount;
    }

    public CreepPower.CreepPowers GetCreepPower()
    {
        return powerType;
    }

    public void SetCreepPower(CreepPower.CreepPowers value)
    {
        powerType = value;
    }

    public void SetLane(SpawnerLane.SpawnLane value)
    {
        lane = value;
    }

    public SpawnerLane.SpawnLane GetLane()
    {
        return lane;
    }

    #endregion

    private float GetStatGain()
    {
        return statGain * upgradeCount;
    }





}
