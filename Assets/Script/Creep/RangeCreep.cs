﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeCreep : Creep
{
    [SerializeField] AIProjectile projectile;
    [SerializeField] GameObject firePoint;

    private AIController controller;

    public event Action<AIProjectile> OnAttack;
    
    public AIProjectile Projectile { get { return projectile; } set { projectile = value; } }
    public GameObject FirePoint { get { return firePoint; } }
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<AIController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RangeAttack()
    {
        AIProjectile casterProjectile = Instantiate(projectile,firePoint.transform.position, firePoint.transform.rotation);

        casterProjectile.SetController(controller);
    }
}
