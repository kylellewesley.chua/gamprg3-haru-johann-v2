﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;
using TMPro;

public class InputMovement : MonoBehaviour
{
    public UnityEvent OnAlpha1Down;
    public UnityEvent OnSelectPlayer;
    public UnityEvent OnDeselectPlayer;

    [SerializeField] Players.PlayerNumber playerNum;
    [SerializeField] Transform moveLocation;
    [SerializeField] LayerMask layer;
    [SerializeField] LayerMask targetLayer;
    

    private PlayerController playerControl;
    private PlayerController playerFinder;
    private int keyCount = 0;
    private float time = 0;
    private float keyTimer = 0.5f;
    private Health selectedHealth;
    private Buildings selectedBuilding;



    private void Awake()
    {
        SingletonManager.Register<InputMovement>(this);

        var players = FindObjectsOfType<PlayerController>();
        foreach(var player in players)
        {
            if(player.PlayerNum == playerNum)
            {
                playerFinder = player;
            }
        }
        
    }


    // Start is called before the first frame update
    void Start()
    {
        if(playerControl == null)
        {
            OnDeselectPlayer?.Invoke();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(keyCount != 0)
        {
            if (Time.time >= time)
            {
                time = Time.time + keyTimer;
                keyCount = 0;
            }
        }
       

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            keyCount++; 

            if(playerControl == null)
            {
                if (selectedHealth)
                    Deselect();

                playerControl = playerFinder;
                playerControl.GetComponent<Health>().IsSelected = true;
                playerControl.GetComponent<Health>().UpdateHealth();
                playerControl.GetComponent<Mana>().UpdateMana();
                OnSelectPlayer?.Invoke();
                
            }

            if(keyCount == 2)
            {
                OnAlpha1Down?.Invoke();
                keyCount = 0;
            }
           

        }


        if (Input.GetMouseButtonDown(0))
        {
            TrySelectingCharacter();

        }

        if (Input.GetMouseButtonDown(1) && playerControl != null)
        {
            MoveToTargetLocation();
        }

        if(moveLocation != null && playerControl != null)
        {
            var distance = Vector3.Distance(moveLocation.position, playerControl.transform.position);
            if(distance <= 1f)
            {
                playerControl.State = PlayerState.State.Idle;
            }
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            if (playerControl != null)
            {
                playerControl.State = PlayerState.State.Idle;
                playerControl.Agent.isStopped = true;
                playerControl.TargetTransform = null;
                playerControl.Agent.ResetPath();
            }
        }
           
    }

    private void MoveToTargetLocation()
    {
        playerControl.State = PlayerState.State.Running;
        ResetDestination();
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity))
        {

            //Movement
            //Have the player move to the raycast/ hit point.
            playerControl.Agent.SetDestination(hit.point);
            moveLocation.position = new Vector3(hit.point.x, moveLocation.position.y, hit.point.z);


            //Rotation
            Quaternion rotationToLookAt = Quaternion.LookRotation(hit.point);
            float rotationY = Mathf.SmoothDampAngle(playerControl.transform.eulerAngles.y, rotationToLookAt.eulerAngles.y,
                ref playerControl.rotateVelocity, playerControl.TurnRate * (Time.deltaTime * 5));

            playerControl.transform.eulerAngles = new Vector3(0, rotationY, 0);

            if(hit.collider.GetComponent<Creep>())
            {
                if(hit.collider.GetComponent<Creep>().GetFaction() != playerControl.Faction)
                {
                    playerControl.TargetTransform = hit.collider.gameObject.transform;
                    playerControl.hasTarget = true;
                }
                    
            }

        }
    }

    private void TrySelectingCharacter()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity))
        {
            Debug.Log(hit.collider);
            var myCharacter = hit.collider.gameObject.GetComponent<PlayerController>();
            if (myCharacter)
            {
                if (selectedHealth)
                {
                    Deselect();
                }

                if (playerNum == myCharacter.PlayerNum)
                {
                    playerControl = myCharacter;
                    playerControl.SelectedSprite.enabled = true;
                    playerControl.GetComponent<Health>().IsSelected = true;
                    playerControl.GetComponent<Health>().UpdateHealth();
                    playerControl.GetComponent<Mana>().UpdateMana();
                    playerControl.UpdateDamageUI();
                    OnSelectPlayer?.Invoke();
                }

            }
            else
            {
                if (playerControl != null)
                {
                    playerControl.SelectedSprite.enabled = false;
                    playerControl.GetComponent<Health>().IsSelected = false;
                    playerControl = null;
                    OnDeselectPlayer?.Invoke();
                }

                var selected = hit.collider.GetComponent<Health>();
                SelectingOtherUnits(selected);
            }
        }
    }

    private void SelectingOtherUnits(Health selected)
    {
        if (selected)
        {
            if (!selectedHealth)
            {
                selectedHealth = selected;
                selectedHealth.IsSelected = true;
                selectedHealth.UpdateHealth();
                selectedHealth.GetComponent<Mana>().UpdateMana();
                CheckBuildingSelected(selectedHealth);
                OnSelectPlayer?.Invoke();
            }
            else if (selected != selectedHealth)
            {
                selectedHealth.IsSelected = false;
                selectedHealth = selected;
                selectedHealth.IsSelected = true;
                selectedHealth.UpdateHealth();
                selectedHealth.GetComponent<Mana>().UpdateMana();
                CheckBuildingSelected(selectedHealth);
                OnSelectPlayer?.Invoke();
            }
        }
        else
        {
            if (selectedHealth)
            {
                Deselect();
            }
            OnDeselectPlayer?.Invoke();
        }
    }

    private void CheckBuildingSelected(Health selected)
    {
        var building = selected.GetComponent<Buildings>();
        if (building)
        {
            selectedBuilding = building;
            selectedBuilding.DestroyButton.gameObject.SetActive(true);
            var destroyButtonText = selectedBuilding.DestroyButton.Find("Destroy Text").GetComponent<TextMeshProUGUI>();
            destroyButtonText.SetText("Destroy " + selectedBuilding.name);
        }
    }

    private void Deselect()
    {
        selectedHealth.IsSelected = false;
        
        if(selectedBuilding != null)
        {
            var destroyButtonText = selectedBuilding.DestroyButton.Find("Destroy Text").GetComponent<TextMeshProUGUI>();
            destroyButtonText.SetText("No Selected Building");
        }
        
        selectedHealth = null;
    }

    private void ResetDestination()
    {
        playerControl.Agent.isStopped = false;
        playerControl.TargetTransform = null;
        
    }

    public void DestroyBuilding()
    {
        if(selectedBuilding != null)
        {
            var buildingToDestroy = selectedBuilding.GetComponent<Health>();
            buildingToDestroy.DestroyThisBuilding();
        }

      
    }
}
