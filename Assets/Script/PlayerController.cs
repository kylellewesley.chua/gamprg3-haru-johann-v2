﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;

public class PlayerState
{
    public enum State
    {
        Idle,
        Turning,
        Running,
        Attack
    };
}

public class PlayerController : MonoBehaviour
{
    [SerializeField] PlayerState.State state;
    [SerializeField] Players.PlayerNumber playerNum;
    [SerializeField] FactionType.FactionTypes faction;
    [SerializeField] ArmorType.Types armorType;
    [SerializeField] AttackType.Types attackType;
    [SerializeField] GameObject playerModel;
    [SerializeField] SpriteRenderer selectedSprite;

    [SerializeField] Transform targetTransform;
    [SerializeField] float targetMaxRadius = 20f;
    [SerializeField] float damage = 40f;
    [SerializeField] float armor = 6f;
    [SerializeField] float turnRate = 0.1f;
    [SerializeField] float attackRange = 1.5f;
    [SerializeField] float attackRate = 2f;
    [SerializeField] TextMeshProUGUI damageText;
    [SerializeField] TextMeshProUGUI armorText;

    private bool canAttack = true;


    [HideInInspector]
    public float rotateVelocity;


    [SerializeField] NavMeshAgent agent;
    [SerializeField] Animator anim;
    private Health health;

    //Properties
    public NavMeshAgent Agent { get { return agent; } }
    public PlayerState.State State { get { return state; } set { state = value; } }
    public float TurnRate { get { return turnRate; } }
    public GameObject PlayerModel { get { return playerModel; } }
    public Players.PlayerNumber PlayerNum { get { return playerNum; } }
    public FactionType.FactionTypes Faction { get { return faction; } }
    public AttackType.Types AttackType { get { return attackType; } }
    public ArmorType.Types ArmorType { get { return armorType; } }
    public SpriteRenderer SelectedSprite { get { return selectedSprite; } }
    public Transform TargetTransform { get { return targetTransform; } set { targetTransform = value; } }
    public float Armor { get { return armor; } }
    public bool CanAttack { set { canAttack = value; } }



    public Vector3 moveLocation { set; get; }
    public bool hasTarget;
    public bool hasControl { set; get; }

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        health = GetComponent<Health>();
        health.OnSelected += Health_OnSelected;
        moveLocation = transform.position;

    }

    private void OnDestroy()
    {
        health.OnSelected -= Health_OnSelected;
    }

    private void Health_OnSelected()
    {
        UpdateDamageUI();
        UpdateArmorUI();
    }


    // Start is called before the first frame update
    void Start()
    {
        //damageText.text = damage.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (targetTransform == null)
            hasTarget = false;

        if(state == PlayerState.State.Idle)
        {
            CheckForTarget(targetTransform);
            if (targetTransform == null)
                LookForTarget();
        }

        if (hasTarget)
        {
            Vector3 direction = targetTransform.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, turnRate * Time.deltaTime);
            agent.SetDestination(targetTransform.position);
            var distance = Vector3.Distance(targetTransform.position, this.transform.position);
            if (distance <= attackRange)
            {
                state = PlayerState.State.Attack;

                if (canAttack)
                    StartCoroutine(Attack());

                agent.isStopped = true;
                agent.ResetPath();
            }
            else
            {
                state = PlayerState.State.Running;
                agent.isStopped = false;
            }


        }
        
    }

    public void UpdateDamageUI()
    {
        int damageFloatToInt = (int)damage;
        damageText.text = damageFloatToInt.ToString();
    }

    public void UpdateArmorUI()
    {
        int armorFloatToInt = (int)GetComponent<Health>().Armor;
        armorText.text = armorFloatToInt.ToString();
    }

    private IEnumerator Attack()
    {
        canAttack = false;
        anim.SetTrigger("Attack");
        yield return new WaitForSeconds(attackRate);
        canAttack = true;
    }

    public void CanDamage()
    {
        var myTarget = targetTransform;
        var distance = Vector3.Distance(myTarget.position, transform.position);
        if (distance <= attackRange)
        {
            DealDamage(myTarget);
        }
    }

    public void DealDamage(Transform target)
    {
        var enemyUnit = target.GetComponent<Health>();
        enemyUnit.OnDamage(damage, this.gameObject);
    }

    public void CheckForTarget(Transform target)
    {
        if (target != null)
        {
            hasTarget = true;
            Vector3 direction = target.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, turnRate * Time.deltaTime);
        }
        else
        {
            hasTarget = false;
            LookForTarget();
        }
    }


    public void LookForTarget()
    {

        Collider[] colliderArray = Physics.OverlapSphere(transform.position, targetMaxRadius);


        foreach (Collider collider in colliderArray)
        {
            PlayerController enemyPlayer = collider.GetComponent<PlayerController>();
            Creep creep = collider.GetComponent<Creep>();
            Buildings building = collider.GetComponent<Buildings>();



            CheckCreepsNearby(creep);
            CheckBuildingsNearby(building);


        }
    }


    private void CheckCreepsNearby(Creep creep)
    {
        if (creep != null)
        {
            //Check if they are not on the same faction
            if (creep.GetFaction() != this.gameObject.GetComponent<PlayerController>().faction)
            {
                if (targetTransform == null)
                {
                    targetTransform = creep.transform;
                }
                else
                {
                    //change to the closest target
                    if (Vector3.Distance(transform.position, creep.transform.position) <
                        Vector3.Distance(transform.position, targetTransform.position))
                    {
                        targetTransform = creep.transform;
                    }
                }
            }
        }
    }


    private void CheckBuildingsNearby(Buildings building)
    {
        if (building != null)
        {
            if (building.GetFactionType() != this.gameObject.GetComponent<PlayerController>().faction)
            {
                if (targetTransform == null)
                {
                    if (building.GetIsVunerable())
                    {
                        targetTransform = building.transform;
                    }

                }
                else
                {
                    //change to the closest target
                    if (Vector3.Distance(transform.position, building.transform.position) <
                    Vector3.Distance(transform.position, targetTransform.position))
                    {
                        targetTransform = building.transform;
                        Debug.Log("Building located: " + targetTransform);
                    }
                }
            }
        }
    }


}



[System.Serializable]
public class Players
{
    public enum PlayerNumber
    {
        Player1,
        Player2,
        Player3
    };
}
