﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    public GameObject timeManager;
    TimeManager tManager;

    public Text UiTextTime;

    public GameObject debugWindow;

    //public GameObject selectedButtonBackground;
    // Start is called before the first frame update
    void Start()
    {
        tManager = timeManager.GetComponent<TimeManager>();
    }

    // Update is called once per frame
    void Update()
    {
        ChangeUITextTime();
    }

    void ChangeUITextTime()
    {
        UiTextTime.text = tManager.minutes.ToString("D2") + ":" +tManager.seconds.ToString("D2");
    }

    public void DebugButtonOnPressed()
    {
        debugWindow.SetActive(!debugWindow.activeSelf);
    }

    public void TimesTwoButtonOnPressed()
    {
       
        if(tManager.timeCoeff == 2)
        {
            tManager.timeCoeff = 1;
           // selectedButtonBackground.SetActive(false);
        }
        else if(tManager.timeCoeff != 2)
        {
            tManager.timeCoeff = 2;
            //selectedButtonBackground.SetActive(true);
           // selectedButtonBackground.transform.localPosition = new Vector3(0, 0, 0);
        }
    }

    public void HalfTwoButtonPressed()
    {
        if (tManager.timeCoeff == 0.5f)
        {
            tManager.timeCoeff = 1;
            //selectedButtonBackground.SetActive(false);
        }
        else if (tManager.timeCoeff != 0.5f)
        {
            tManager.timeCoeff = 0.5f;
           // selectedButtonBackground.SetActive(true);
           // selectedButtonBackground.transform.localPosition = new Vector3(125, 0, 0);

        }
    }
}
