﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    [SerializeField] PlayerController player;
    [SerializeField] Transform spawnLocation;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RespawnCharacter()
    {
        if (!player.gameObject.activeInHierarchy)
        {
            player.gameObject.SetActive(true);
            player.CanAttack = true;
        }
        player.GetComponent<Health>().SetHealth();
        player.transform.position = spawnLocation.position;
        player.TargetTransform = null;


    }
}
