﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

[System.Serializable]
public class OnBuildingDestroyEvent : UnityEvent<Buildings> { }


[SerializeField]
public class BuildingType
{
    public enum BuildingTypes
    {
        MeleeBarracks,
        RangeBarracks,
        Tower,
        Core
    }
}


public class Buildings : MonoBehaviour
{
    public OnBuildingDestroyEvent OnBuildingDestroy;

    [SerializeField] private float towersLeft;
    [SerializeField] private FactionType.FactionTypes factionTypes;
    [SerializeField] private BuildingType.BuildingTypes types;
    [SerializeField] private ArmorType.Types armorType;
    [SerializeField] private AttackType.Types attackType;
    [SerializeField] TowerProjectile projectilePrefab;
    [SerializeField] GameObject towerMuzzle;
    [SerializeField] GameObject firePoint;
    [SerializeField] private bool isVunerable = false;
    [SerializeField] private float health = 2000;
    [SerializeField] private float damage = 0;
    [SerializeField] private float shootTimerMax;
    private float shootTimer;
    [SerializeField] private float armor = 10f;
    [SerializeField] private float bountyReward = 200f;
    [SerializeField] private float expReward = 50f;
    [SerializeField] private Transform targetTransform;
    [SerializeField] TextMeshProUGUI damageText;
    [SerializeField] TextMeshProUGUI armorText;
    [SerializeField] Transform destroyButton;

    public Transform DestroyButton { get { return destroyButton; } }

    private bool hasTarget;
    private float targetMaxRadius = 5f;

    private Health healthRef;
    private Canvas canvasUI;

    private void Awake()
    {
        UIReferenceSetup();
        healthRef = GetComponent<Health>();
        healthRef.OnSelected += HealthRef_OnSelected;
    }

    private void OnDestroy()
    {
        healthRef.OnSelected -= HealthRef_OnSelected;
    }

    private void HealthRef_OnSelected()
    {
        UpdateDamageUI();
        UpdateArmorUI();
    }

    private void UIReferenceSetup()
    {
        canvasUI = FindObjectOfType<Canvas>();
        damageText = canvasUI.transform.Find("damageText")
            .transform.Find("damageTemplate")
            .transform.Find("damageAmount").GetComponent<TextMeshProUGUI>();
        armorText = canvasUI.transform.Find("armorText")
            .transform.Find("armorTemplate")
            .transform.Find("armorAmount").GetComponent<TextMeshProUGUI>();
    }

    public void UpdateDamageUI()
    {
        int damageFloatToInt = (int)damage;
        damageText.text = damageFloatToInt.ToString();
    }

    public void UpdateArmorUI()
    {
        int armorFloatToInt = (int)armor;
        armorText.text = armorFloatToInt.ToString();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (types == BuildingType.BuildingTypes.Tower)
        {
            CheckForTarget(targetTransform);
            if (targetTransform == null)
            {
                LookForTarget();
            }

            if (targetTransform != null)
            {
                var distance = Vector3.Distance(targetTransform.position, this.transform.position);
                Vector3 direction = targetTransform.position - towerMuzzle.transform.position;
                Quaternion rotation = Quaternion.LookRotation(direction);
                towerMuzzle.transform.rotation = Quaternion.Slerp(towerMuzzle.transform.rotation, rotation, 20f * Time.deltaTime);

                if (distance > targetMaxRadius)
                {
                    targetTransform = null;
                }
            }
        }


        if (types == BuildingType.BuildingTypes.Core)
        {
            if (towersLeft <= 0)
            {
                isVunerable = true;
            }
        }


    }

    public void TowerAttack()
    {
        TowerProjectile casterProjectile = Instantiate(projectilePrefab, firePoint.transform.position, firePoint.transform.rotation);

        casterProjectile.SetController(this);
    }

    private void HandleShooting()
    {
        shootTimer -= Time.deltaTime;
        if (shootTimer <= 0f)
        {
            shootTimer += shootTimerMax;
            if (targetTransform != null)
            {
                if (types == BuildingType.BuildingTypes.Tower)
                {
                    TowerAttack();
                    //GetComponent<ProjectilePrefab>().GetFirePoint().LookAt(targetTransform);
                    //GetComponent<ProjectilePrefab>().FireTowerProjectile();
                }

            }
        }

    }

    public void CheckForTarget(Transform target)
    {
        if (target != null)
        {
            hasTarget = true;
            HandleShooting();

        }
        else
        {
            hasTarget = false;
            LookForTarget();
        }
    }


    public void LookForTarget()
    {

        Collider[] colliderArray = Physics.OverlapSphere(transform.position, targetMaxRadius);


        foreach (Collider collider in colliderArray)
        {
            Creep creep = collider.GetComponent<Creep>();
            PlayerController player = collider.GetComponent<PlayerController>();


            CheckCreepsNearby(creep);
            CheckHeroesNearby(player);


        }
    }


    private void CheckCreepsNearby(Creep creep)
    {
        if (creep != null)
        {
            //Check if they are not on the same faction
            if (creep.GetFaction() != this.gameObject.GetComponent<Buildings>().GetFactionType())
            {
                if (targetTransform == null)
                {
                    targetTransform = creep.transform;
                }
                else
                {
                    //change to the closest target
                    if (Vector3.Distance(transform.position, creep.transform.position) <
                        Vector3.Distance(transform.position, targetTransform.position))
                    {
                        targetTransform = creep.transform;
                    }
                }
            }
        }
    }

    private void CheckHeroesNearby(PlayerController enemyPlayer)
    {
        if (enemyPlayer != null)
        {
            //Check if they are not on the same faction
            if (enemyPlayer.Faction != this.gameObject.GetComponent<Buildings>().GetFactionType())
            {
                if (targetTransform == null)
                {
                    targetTransform = enemyPlayer.transform;
                }
                else
                {
                    //change to the closest target
                    if (Vector3.Distance(transform.position, enemyPlayer.transform.position) <
                        Vector3.Distance(transform.position, targetTransform.position))
                    {
                        targetTransform = enemyPlayer.transform;
                    }
                }
            }
        }
    }



    public void RemoveTower()
    {
        towersLeft--;
    }


    public FactionType.FactionTypes GetFactionType()
    {
        return factionTypes;
    }

    public BuildingType.BuildingTypes GetBuildingType()
    {
        return types;
    }

    public ArmorType.Types GetArmorType()
    {
        return armorType;
    }

    public AttackType.Types GetAttackType()
    {
        return attackType;
    }

    public bool GetIsVunerable()
    {
        return isVunerable;
    }

    public void SetIsVunerable(bool value)
    {
        isVunerable = value;
    }

    public float GetHealth()
    {
        return health;
    }

    public float GetDamage()
    {
        return damage;
    }

    public float GetArmor()
    {
        return armor;
    }

    public float GetBountyReward()
    {
        return Random.Range(bountyReward - 20, bountyReward);
    }

    public float GetExpReward()
    {
        return expReward;
    }

    public Transform GetTargetTransform()
    {
        return targetTransform;
    }

    public void SetTargetTransformToNull()
    {
        targetTransform = null;
    }

    public void OnDie()
    {
        OnBuildingDestroy?.Invoke(this);
        gameObject.SetActive(false);
        Invoke("Destroy", 3f);
    }


    private void Destroy()
    {
        Destroy(this.gameObject);
    }

}
