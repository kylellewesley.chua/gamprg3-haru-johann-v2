﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraHandler : MonoBehaviour
{

    [SerializeField] float panSpeed = 2f;
    [SerializeField] float zoomSpeed = 3f;
    [SerializeField] float zoomInMax = 10f;
    [SerializeField] float zoomOutMax = 50f;
    [SerializeField] float panBorderThickness = 10f;
    [SerializeField] PlayerController playerControl;

    private CinemachineVirtualCamera cinemachineVirtualCamera;
    private Transform cameraTransform;




    // private float zoomY;

    private void Awake()
    {
        cinemachineVirtualCamera = GetComponent<CinemachineVirtualCamera>();
        cameraTransform = cinemachineVirtualCamera.VirtualCameraGameObject.transform;
    }

    // Start is called before the first frame update
    void Start()
    {

        //zoomY = cinemachineVirtualCamera.gameObject.transform.position.y;
    }


    // Update is called once per frame
    void Update()
    {
        float x = Input.mousePosition.x;
        float y = Input.mousePosition.y;
        float z = Input.mouseScrollDelta.y;

        //Vector3 pos = transform.position;
        //if(y >= Screen.height + panBorderThickness)
        //{
        //    pos.z += panSpeed * Time.deltaTime;
        //}
        //if(y <= panBorderThickness)
        //{
        //    pos.z -= panSpeed * Time.deltaTime;
        //}
        //if(x  >= Screen.width - panBorderThickness)
        //{
        //    pos.x += panSpeed * Time.deltaTime;
        //}
        //if(x >= Screen.width - panBorderThickness)
        //{
        //    pos.x -= panSpeed * Time.deltaTime;
        //}


        //transform.position = pos;




        if (x != 0 || y != 0)
        {
            PanScreen(y, x);
        }
        if (z != 0)
        {
            ZoomScreen(z);
        }

        //if (cinemachineVirtualCamera == null)
        //    return;



        //CameraZoom();

    }

    public void ZoomScreen(float increment)
    {
        float fov = cinemachineVirtualCamera.m_Lens.FieldOfView;
        float target = Mathf.Clamp(fov + increment, zoomInMax, zoomOutMax);
        cinemachineVirtualCamera.m_Lens.FieldOfView = Mathf.Lerp(fov, target, zoomSpeed * Time.deltaTime);







        //float posY = cameraTransform.position.y;
        //float targetY = Mathf.Clamp(posY + increment, zoomInMax, zoomOutMax);
        //Vector3 cameraPos = new Vector3(cameraTransform.position.x, posY, cameraTransform.position.z);
        //Vector3 newCameraPos = new Vector3(cameraTransform.position.x, posY + targetY , cameraTransform.position.z);

        //cameraTransform.position = Vector3.Lerp(cameraPos, newCameraPos, zoomSpeed * Time.deltaTime);


    }

    public Vector3 PanDirection(float x, float y)
    {

        Vector3 direction = Vector3.zero;
        if (y >= Screen.width * .95f)
        {
            direction.z += 1;
        }
        if (y <= Screen.width * 0.05f)
        {
            direction.z -= 1;
        }
        if (x >= Screen.height * .95f)
        {
            direction.x -= 1;
        }
        if (x <= Screen.height * 0.05f)
        {
            direction.x += 1;
        }
        return direction;

    }


    public void PanScreen(float x, float y)
    {
        Vector3 direction = PanDirection(x, y);

        cameraTransform.position = Vector3.Lerp(cameraTransform.position, cameraTransform.position + direction * panSpeed, Time.deltaTime);
    }

    public void CameraFocus()
    {
        if (cinemachineVirtualCamera.m_Follow == null && cinemachineVirtualCamera.m_LookAt == null)
            StartCoroutine(AtLookPlayer());

    }

    private IEnumerator AtLookPlayer()
    {
        cinemachineVirtualCamera.m_Follow = playerControl.transform;
        cinemachineVirtualCamera.m_LookAt = playerControl.transform;

        yield return new WaitForSeconds(0.5f);

        cinemachineVirtualCamera.m_Follow = null;
        cinemachineVirtualCamera.m_LookAt = null;
    }





}
