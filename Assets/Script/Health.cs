﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public UnityEvent OnDie;

    [SerializeField] float healthCurrent;
    [SerializeField] float healthMax;
    [SerializeField] float hpPercentage;
    [SerializeField] float armor;
    [SerializeField] RectTransform healthUI;
    [SerializeField] TextMeshProUGUI textHealthCurrent;
    [SerializeField] TextMeshProUGUI textHealthMax;

    private bool hasProtectionBuff = false;
    private bool isSelected = false;
    private ArmorType.Types unitArmorType;
    private AttackType.Types attackerType;
    private FactionType.FactionTypes faction;

    private Canvas canvasUI;

    public event Action OnSelected;

    public bool IsSelected { get { return isSelected; } set { isSelected = value; } }

    public bool HasProtectionBuff { get { return hasProtectionBuff; } set { hasProtectionBuff = value; } }
    public FactionType.FactionTypes Faction { get { return faction; } }
    public float Armor { get { return armor; } }

    public float MyHealthCurrent { get { return healthCurrent; } set { MyHealthCurrent = value; } }


    private void Awake()
    {
        UIReferenceSetup();

        if (this.GetComponent<Creep>())
        {
            healthMax = GetComponent<Creep>().GetHealth();
            armor = GetComponent<Creep>().GetArmor();
            unitArmorType = GetComponent<Creep>().GetArmorType();
            faction = GetComponent<Creep>().GetFaction();
            healthCurrent = healthMax;
        }

        if (this.GetComponent<Buildings>())
        {
            healthMax = GetComponent<Buildings>().GetHealth();
            armor = GetComponent<Buildings>().GetArmor();
            unitArmorType = GetComponent<Buildings>().GetArmorType();
            healthCurrent = healthMax;
        }

        if (this.GetComponent<PlayerController>())
        {
            unitArmorType = GetComponent<PlayerController>().ArmorType;
            armor = GetComponent<PlayerController>().Armor;
            faction = GetComponent<PlayerController>().Faction;
        }
    }

    private void UIReferenceSetup()
    {
        canvasUI = FindObjectOfType<Canvas>();
        healthUI = canvasUI.transform.Find("HealthBar").GetComponent<RectTransform>();
        textHealthCurrent = canvasUI.transform.Find("healthText")
            .transform.Find("healthTemplate")
            .transform.Find("healthCurrent").GetComponent<TextMeshProUGUI>();
        textHealthMax = canvasUI.transform.Find("healthText")
            .transform.Find("healthTemplate")
            .transform.Find("healthMax").GetComponent<TextMeshProUGUI>();
    }

    // Start is called before the first frame update
    void Start()
    {
        SetHealth();
        UpdateHealth();
    }

    public void SetHealth()
    {
        healthCurrent = healthMax;
        hpPercentage = healthCurrent / healthMax;

    }

    public void UpdateHealth()
    {
        if (this.IsSelected)
        {
            if (healthCurrent <= 0)
                healthCurrent = 0;
            hpPercentage = healthCurrent / healthMax;
            int current = (int)healthCurrent;
            int max = (int)healthMax;
            textHealthCurrent.text = current.ToString();
            textHealthMax.text = max.ToString();
            healthUI.localScale = new Vector3(hpPercentage, 1, 1);
            this.OnSelected?.Invoke();
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    public void IncreaseArmor(float armorBonus)
    {
        armor += armorBonus;
        var creepArmor = GetComponent<AIController>();
        var playerArmor = GetComponent<PlayerController>();
        if (creepArmor && IsSelected)
        {
            creepArmor.UpdateArmorUI();
        }
        if (playerArmor && IsSelected)
        {
            playerArmor.UpdateArmorUI();
        }
    }

    public void DecreaseArmor(float armorBonus)
    {
        armor -= armorBonus;
        var creepArmor = GetComponent<AIController>();
        var playerArmor = GetComponent<PlayerController>();
        if (creepArmor && IsSelected)
        {
            creepArmor.UpdateArmorUI();
        }
        if (playerArmor && IsSelected)
        {
            playerArmor.UpdateArmorUI();
        }
    }


    public void OnDamage(float damage, GameObject objectThatDealtDamage)
    {
        AttackerTypeChecker(objectThatDealtDamage);

        var damageMultiplier = 1 - ((0.052f * armor) / (0.9f + 0.048f * Mathf.Abs(armor)));
        damage *= damageMultiplier;
        damage *= DamagePercentage(this.unitArmorType, attackerType);
        healthCurrent -= damage;
        hpPercentage = healthCurrent / healthMax;
        DamagePopup.Create(this.transform.position, (int)damage);
        UpdateHealth();

        if (healthCurrent <= 0)
        {
            healthCurrent = 0;
            if (GetComponent<PlayerController>())
            {
                gameObject.SetActive(false);
                gameObject.transform.position = new Vector3(0, 11.23f, 0);
            }
            else
            {
                OnDie?.Invoke();
                Destroy(gameObject);
            }
               

           
        }

    }

    public void DestroyThisBuilding()
    {
        OnDie?.Invoke();
        Destroy(gameObject);
    }

    private void AttackerTypeChecker(GameObject objectThatDealtDamage)
    {
        if (objectThatDealtDamage.GetComponent<Creep>())
        {
            attackerType = objectThatDealtDamage.GetComponent<Creep>().GetAttackType();
        }

        if (objectThatDealtDamage.GetComponent<Buildings>())
        {
            attackerType = objectThatDealtDamage.GetComponent<Buildings>().GetAttackType();
        }

        if (objectThatDealtDamage.GetComponent<PlayerController>())
        {
            attackerType = objectThatDealtDamage.GetComponent<PlayerController>().AttackType;
        }
    }

    private float DamagePercentage(ArmorType.Types objectThatIsDamaged, AttackType.Types objectThatDealtDamage)
    {
        var armorType = objectThatIsDamaged;
        var attackType = objectThatDealtDamage;

        switch (attackType)
        {
            case AttackType.Types.Basic:
                {
                    if (armorType == ArmorType.Types.Basic)
                        return 1f;
                    else if (armorType == ArmorType.Types.Fortified)
                        return .7f;
                    else if (armorType == ArmorType.Types.Hero)
                        return .75f;
                    break;
                }
                

            case AttackType.Types.Pierce:
                {
                    if (armorType == ArmorType.Types.Basic)
                        return 1.5f;
                    else if (armorType == ArmorType.Types.Fortified)
                        return .35f;
                    else if (armorType == ArmorType.Types.Hero)
                        return .5f;
                    break;
                }
                

            case AttackType.Types.Siege:
                {
                    if (armorType == ArmorType.Types.Basic)
                        return 1f;
                    else if (armorType == ArmorType.Types.Fortified)
                        return 2.5f;
                    else if (armorType == ArmorType.Types.Hero)
                        return 1f;
                    break;
                }
                

            case AttackType.Types.Hero:
                {
                    if (armorType == ArmorType.Types.Basic)
                        return 1f;
                    else if (armorType == ArmorType.Types.Fortified)
                        return .5f;
                    else if (armorType == ArmorType.Types.Hero)
                        return 1f;
                    break;
                }
        }
        return 0;
    }

}
