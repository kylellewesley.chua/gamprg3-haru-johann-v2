﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerProjectile : Projectile
{
    [SerializeField] Buildings projectileCaster;

    private Rigidbody rb;

    private Transform target;
    private bool isReady = false;
    private bool isShot = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (projectileCaster == null)
        {
            Destroy(gameObject);
            return;
        }

        if (isReady)
        {
            //transform.LookAt(target);
            rb.velocity = transform.forward * speed;

            if (target != null)
            {
                var targetRotation = Quaternion.LookRotation(target.position - transform.position);

                rb.MoveRotation(Quaternion.RotateTowards(transform.rotation, targetRotation, turn));
            }
            else
            {
                Destroy(gameObject);
            }


        }
    }

    public void SetController(Buildings value)
    {
        projectileCaster = value;
        damage = projectileCaster.GetDamage();
        target = projectileCaster.GetTargetTransform();
        isReady = true;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (target != null)
            if (other.name == target.name)
            {
                other.GetComponent<Health>().OnDamage(damage, projectileCaster.gameObject);
                Destroy(gameObject);
            }
    }


}

